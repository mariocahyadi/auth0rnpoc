# React Native App With Auth0 (POC)

## DESCRIPTION

This is a project created for Hack Day 2021.

Mobile app build with React Native that utilizing auth0 features such as Authentication and Management API for Users Management.

## INSTALLATIOn

After git clone of this repository, then go to project folder and Run

`yarn install` or `npm install`

copy `.env.sample` file into `.env`

Edit `.env`, and get this value from auth0.com (Applications > Application)
 - AUTH0_DOMAIN
 - AUTH0_CLIENT_ID

## RUNNING APP

Running Android App

`npx react-native run-android`

## TUTORIALS

- [Auth0 for React Native Guide](https://auth0.com/docs/quickstart/native/react-native)

## DEVELOPERS

- Cameng Adi Saputra
- Mario Cahyadi

