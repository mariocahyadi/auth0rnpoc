import Config from 'react-native-config';

module.exports = {
  clientId: Config.AUTH0_CLIENT_ID,
  domain: Config.AUTH0_DOMAIN,
};
